Configurações necessárias:

1) Registro no Einstein Platform Services: https://api.einstein.ai/signup
2) Baixar o einstein_platform.csv com o conteúdo da chave de acesso
3) No Salesforce anexar o csv (Item 2) com o Título 'einstein_platform' OU 'predictive_services';
4) Alterar na classe EinsteinAPIService, o subscrition para o e-mail cadastrado no item 1 e o dominio para o da org que foram executar as chamadas;
5) Configurar o remote sites com o site do item 1;
