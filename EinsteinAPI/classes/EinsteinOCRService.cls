public class EinsteinOCRService {
    public static FINAL String  OCR_API         = 'https://api.einstein.ai/v2/vision/ocr';
    public static FINAL String  OCR_MODEL       = 'OCRModel';
    public static FINAL String  OCR_MODEL_TABEL = 'OCRModel';
    
    public static void readTextFromImageByURL(String sampleURL){
        	String sample = '';
        if(sampleURL == ''){
        	sample = 'https://i1.wp.com/www.sfdcpanther.com/wp-content/uploads/2020/07/Day-1.png';
        } else {
            sample = sampleURL;
        }
        String result = EinsteinAPIService.imageOCR(OCR_API, sample, OCR_MODEL, false);
        parseResponse(result);
    }
    
    public static void readTextFromImageByBase64(Id documentId){
        List<ContentDocumentLink> contentLink = [SELECT ContentDocumentId, LinkedEntityId  
                                                 FROM ContentDocumentLink where ContentDocumentId  =:documentId];
// replace 0010o00002KIY2SAAX with the RecordId where you have uploaded the file
        if(!contentLink.isEmpty()){
            ContentVersion content = [SELECT Title,VersionData FROM 
                                      ContentVersion 
                                      where ContentDocumentId =: contentLink.get(0).ContentDocumentId 
                                      LIMIT 1];
            String sample = EncodingUtil.base64Encode(content.VersionData);
            String result = EinsteinAPIService.imageOCR(OCR_API, sample, OCR_MODEL, true);
            parseResponse(result);
        }
    }
    private static void parseResponse(String result){
        EinsteinOCRResponse response = (EinsteinOCRResponse)System.JSON.deserialize(result, EinsteinOCRResponse.class);
        List<String> namesToSearch = new List<String>();
        Integer i = 0;
        for(EinsteinOCRResponse.Probabilities prob : response.probabilities){
            //min x =48 min, y = 103 | max x = 268, max y=126
            /*if(prob.boundingBox.minX >= 48 && prob.boundingBox.maxX <= 268 && prob.boundingBox.minY >= 103 && prob.boundingBox.maxY <= 126){
            	System.debug(System.LoggingLevel.DEBUG, 'O valor: ' + i + ' ' + prob.label + ' probabilidade: ' + prob.probability);
                namesToSearch.add('%'+prob.label+'%');
            }*/
            System.debug(System.LoggingLevel.DEBUG, 'O valor: ' + i + ' ' + prob.label + ' probabilidade: ' + prob.probability);
        }
        System.debug(namesToSearch);
        List<Contact> searchContacts = [SELECT Id, Name FROM Contact WHERE Name LIKE :namesToSearch];
        System.debug('Contatos: '+searchContacts);
        
    }
}